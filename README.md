#  Accéder à une Base de Données grâce à Java

Le repo possède un **PDF** explicatif ainsi que tout le nécessaire pour se connecter à une BDD, insérer de nouvelles entrées, en supprimer et créer un utilisateur avec les données présentes dans la BDD. 
Le **PDF** contient les sections suivantes :
- Qu'est une ***Base de Données***.
- Installer ***DB Browser*** pour SQLite.
- Qu'est-ce que ***JDBC***.
- Utiliser ***JDBC*** dans IntelliJ.
