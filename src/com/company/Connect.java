package com.company;

import java.util.Scanner;

public class Connect {
    /**
     * Connect to a sample database
     */

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        HelloDB helloDB = new HelloDB();
        helloDB.connect();

        boolean fini = false;
        int input = 0;

        do {
            // si on affiche un objet, on n'affiche pas la bdd
            if(input != 3) {
                helloDB.readDB();
            }

            System.out.println("Que voulez vous faire ?");
            System.out.println("1 : Entrer une autre personne");
            System.out.println("2 : Supprimer une personne");
            System.out.println("3 : Créer une instance de Person");
            System.out.println("4 : Terminer");

            Scanner userInput = new Scanner(System.in);
            input = userInput.nextInt();

            switch(input) {
                case 1:
                    helloDB.writeInDB();
                    break;
                case 2:
                    helloDB.deleteInDB();
                    break;
                case 3:
                    helloDB.populateWithDbEntry();
                    break;
                default:
                    fini = true;
            }

        }while (!fini);

    }
}