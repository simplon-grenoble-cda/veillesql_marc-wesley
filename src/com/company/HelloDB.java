package com.company;

import java.sql.*;
import java.util.Scanner;

public class HelloDB
{

    private Statement stmt;
    private final String tableName = "Person"; // <= Indiquer ici le nom de la table

    public void connect() throws Exception {

        // Enregistrer le driver JDBC
        String sDriverName = "org.sqlite.JDBC";
         Class.forName(sDriverName);

        String sTempDb = "db/test.db";  // <= Indiquer ici le nom et chemin de votre db
        String sJdbc = "jdbc:sqlite";
        String sDbUrl = sJdbc + ":" + sTempDb;

        // create a database connection
        Connection conn = DriverManager.getConnection(sDbUrl);

        try {
            this.stmt = conn.createStatement();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    };

    public void readDB () {

        try {
            // SELECT * => lire toute la table
            String sMakeSelect = "SELECT * FROM " + tableName;
            ResultSet rs = stmt.executeQuery(sMakeSelect);
            // pour chaque enregistrement afficher les données
            while(rs.next()) {
                String firstname = rs.getString("Firstname");
                String lastname = rs.getString("Lastname");
                int id = rs.getInt("id");
                int age = rs.getInt("Age");

                System.out.println("ID number: " + id + " = " + firstname + " " + lastname + " à " + age + " ans.");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void writeInDB () {

        // Add Lastname into database
        Scanner inputLastname = new Scanner(System.in);
        System.out.println("Veuillez entrer votre nom :");
        String responseLastname = inputLastname.nextLine();

        // Add Firstname into database
        Scanner inputFirstname = new Scanner(System.in);
        System.out.println("Veuillez entrer votre prénom :");
        String responseFirstname = inputFirstname.nextLine();

        // Add Age into database
        Scanner inputAge = new Scanner(System.in);
        System.out.println("Veuillez entrer votre age :");
        int responseAge = inputAge.nextInt();

        // INSERT INTO => créer un enregistrement dans la table
        String sInsert = "INSERT INTO " + tableName + " (Lastname, Firstname, Age) VALUES ('" + responseLastname + "','"+ responseFirstname + "'," + responseAge + ")";

        try {
            int rs = this.stmt.executeUpdate(sInsert);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public void deleteInDB () {

        // rentrer l'id de la ligne à supprimer
        Scanner inputId = new Scanner(System.in);
        System.out.println("Quelle ligne souhaitez-vous supprimer ?");
        int responseDeleteId = inputId.nextInt();

        try {
            // DELETE FROM => supprimer dans la table, WHERE => condition quel enregistrement supprimer
            String sDelete = "DELETE FROM " + tableName + " WHERE `id` = " + responseDeleteId ;
            boolean rs = stmt.execute(sDelete);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void populateWithDbEntry() {
        this.readDB();

        Scanner chooseInput = new Scanner(System.in);
        System.out.println("Quel id souhaitez-vous utiliser pour créer votre utilisateur ?");
        int answerInput = chooseInput.nextInt();

        // Récupérer les valeurs présentes dans la db avec l'ID spécifié
        try {
            String sMakeSelect = "SELECT * FROM Person WHERE ID IN ("+ answerInput +")";
            ResultSet rs = stmt.executeQuery(sMakeSelect);
            while(rs.next()) {
                int id = rs.getInt("id");
                String firstname = rs.getString("Firstname");
                String lastname = rs.getString("Lastname");
                int age = rs.getInt("Age");

                // on crée une instance de Person avec le résultat de la requete
                Person person = new Person(id, firstname, lastname, age);

                // on affiche les détails de l'objet
                System.out.println("Voici mon objet Person");
                System.out.println(person.getId() + " : " + person.getFirstname() + " " + person.getLastname() + " " + person.getAge() + " an(s)");

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    };

}